# cypress-basic-course

[![pipeline status](https://gitlab.com/nandozamai/curso-cypress/badges/master/pipeline.svg)](https://gitlab.com/nandozamai/curso-cypress/-/commits/master)

[Basic course of test automation with Cypress](http://udemy.com/course/test-automation-with-cypress-basic) from the Talking About Testing school.